import React, {useEffect, useState} from 'react';
import {DragDropContext, DraggableLocation, Droppable, DroppableProvided, DropResult} from 'react-beautiful-dnd';
import * as data from '../tasks.json';
import Card from "./Card";

import {CardInterface} from "../interfaces/CardInterface";


function App() {
    const [todoCards, setTodoCards] = useState<CardInterface[]>([]);
    const [doneCards, setDoneCards] = useState<CardInterface[]>([]);

    const cardsArray: any = data.tasks;

    useEffect(() => {
        let todo: CardInterface[] = [], done: CardInterface[] = [];
        cardsArray.map((task: CardInterface) => {
            todo = task.status === 'todo' ? [...todo, task] : todo;
            done = task.status === 'done' ? [...done, task] : done;
        })

        setTodoCards(todo);
        setDoneCards(done);
    }, []);

    const getCards = (idList: string) => {
        return idList === 'todo' ? todoCards : doneCards;
    };

    const toMove = (
        source: any,
        destination: any,
        droppableDestination: DraggableLocation,
        droppableSource: DraggableLocation
    ) => {
        const sourceClone = source.slice();
        const destinationClone = destination.slice();
        const [removed] = sourceClone.splice(droppableSource.index, 1);
    
        destinationClone.splice(droppableDestination.index, 0, removed);
    
        return {
            [droppableSource.droppableId]: sourceClone,
            [droppableDestination.droppableId]: destinationClone
        };
    };
    
    const onDragEnd = (result: DropResult) => {
        const {source, destination} = result;
        if (!destination) {
            return;
        }
        if (destination.index === source.index && destination.droppableId === source.droppableId) {
          return
        }

        const resultArray = toMove(getCards(source.droppableId), getCards(destination.droppableId), destination, source);
        setTodoCards(resultArray.todo);
        setDoneCards(resultArray.done);
    };

    return (
        <div className="wrapper">
            <DragDropContext onDragEnd={onDragEnd}>
                <div>
                    <h2>Todo</h2>
                    <Droppable droppableId='todo'>
                        {(provided: DroppableProvided) => (
                            <div ref={provided.innerRef} className="column"><Card type='todo' cards={todoCards}/></div>
                        )}
                    </Droppable>
                </div>
                <div>
                    <h2>Done</h2>
                    <Droppable droppableId='done'>
                        {(provided: DroppableProvided) => (
                            <div className="column" ref={provided.innerRef}><Card type='done' cards={doneCards}/></div>
                        )}
                    </Droppable>
                </div>
            </DragDropContext>
        </div>
    )
}

export default App;
