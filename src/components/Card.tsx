import React from 'react';
import {Draggable, DraggableProvided,} from 'react-beautiful-dnd';

import {CardInterface} from "../interfaces/CardInterface";

const Card = (props: { type: string, cards: CardInterface[] }) => {
    return (
    <>
        {(props.cards.map((card: CardInterface, index: number) => {
            return <Draggable key={card.id} draggableId={card.id} index={index}>
                {(provided: DraggableProvided) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        className="card"
                    >
                        <p style={props.type === 'done' ? {textDecoration: "line-through"} : {}}>{card.title}</p>
                    </div>
                )}
            </Draggable>
        }))}
    </>
    )
};

export default Card;
