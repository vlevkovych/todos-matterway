export interface CardInterface {
    title: string;
    status: string;
    id: string;
}
